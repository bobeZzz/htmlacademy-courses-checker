﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace courses_check
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class Course
        {
            public int Id;
            public bool IsDone;
            public string Name;
            public string Link;

            public Course(int id)
            {
                Id = id;
                Link = "https://htmlacademy.ru/courses/" + id;
            }
        };

        public static List<Course> courseIdList;
        public static string username;
        public MainWindow()
        {
            InitializeComponent();
            courseIdList = new List<Course>();

            InitializeCourseList();
        }

        private void SendRequestButton_Click(object sender, RoutedEventArgs e)
        {
            ResultLabel.Background = Brushes.White;
            ResultLabel.Content = "n/16";
            int result = CheckAccount(IdTextBox.Text);
            ResultLabel.Content = result + "/16";
            UsernameLabel.Content = username;
            if (result == 16)
                ResultLabel.Background = Brushes.Green;
            else
                ResultLabel.Background = Brushes.Red;
        }

        public void InitializeCourseList()
        {
            courseIdList.Add(new Course(187));
            courseIdList.Add(new Course(130));
            courseIdList.Add(new Course(26));
            courseIdList.Add(new Course(38));
            courseIdList.Add(new Course(40));
            courseIdList.Add(new Course(39));
            courseIdList.Add(new Course(46));
            courseIdList.Add(new Course(41));
            courseIdList.Add(new Course(42));
            courseIdList.Add(new Course(43));
            courseIdList.Add(new Course(44));
            courseIdList.Add(new Course(53));
            courseIdList.Add(new Course(45));
            courseIdList.Add(new Course(66));
            courseIdList.Add(new Course(50));
            courseIdList.Add(new Course(55));
        }

        public static int CheckAccount(string id)
        {
            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;
            string html = wc.DownloadString("https://htmlacademy.ru/profile/" + id + "/progress");
            Regex tableRegex = new Regex(@"<table class=\42courses-table\42>(.*?)<\/table>");
            Match tableMatch = tableRegex.Match(html);
            string table = tableMatch.Value;
            int done = 0;


            for (int i = 0; i < courseIdList.Count; i++)
            {
                //<a class="courses-table__link" href=\42\/courses\/{ID}\42>(.*\s)<\/a(?:.\s?)*?(?:.\s?)*?<span class=\42badge badge--green\42>
                Regex courseRegex = new Regex(@"<a class=\42courses-table__link\42 href=\42\/courses\/" + courseIdList[i].Id + @"\42>((?:.?\s?)*?)<\/a(?:.\s?)*?(?:.\s?)*?<span class=\42(.*?)\42>");
                Match courseMatch;
                courseMatch = courseRegex.Match(table);
                if (courseMatch.Success)
                {
                    string badge = courseMatch.Groups[2].Value;
                    courseIdList[i].Name = courseMatch.Groups[1].Value;



                    if (badge.Contains("badge--green"))
                    {
                        courseIdList[i].IsDone = true;
                        done++;
                    }
                }


            }


            return done;


        }
    }
}
